package ie.atu.shopping_cart;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTests {

    private ShoppingCart ShoppingCart;

    @BeforeEach
    void setUp() {
         ShoppingCart = new ShoppingCart("Book", 10.99,1);
    }

    @Test
    public void testGetName(){
        assertEquals("Book", ShoppingCart.getName(), "Item name should match");
    }

    @Test
    public void testGetPrice(){
        assertEquals(10.99, ShoppingCart.getPrice(), "Item price should match");
    }

    @Test
    public void testSetPriceValid(){
        ShoppingCart item = new ShoppingCart();
        double validPrice = 10.0;

        item.setPrice(validPrice);

        assertEquals(validPrice, item.getPrice(), 0.001); // delta for double comparison
    }
    @Test
    public void testGetId(){
        assertEquals(1, ShoppingCart.getId(), "Item count should match");
    }

    @Test
    void testNameFail(){
        Exception exMessage = assertThrows(IllegalArgumentException.class, () -> {new ShoppingCart("Bo", 15.99);});
        assertEquals("This is not a valid name. All Item contains names of 3 letters or above can be added", exMessage.getMessage());
    }

    @Test
    void testPriceFail(){
        Exception exMessage = assertThrows(IllegalArgumentException.class, () -> {new ShoppingCart("Book", 0);});
        assertEquals("This is not a valid price. All Item prices above 0 can be added ", exMessage.getMessage());
    }

    @AfterEach
    void tearDown() {
    }
}