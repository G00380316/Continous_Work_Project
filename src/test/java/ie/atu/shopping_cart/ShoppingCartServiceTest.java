package ie.atu.shopping_cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ShoppingCartServiceTest {

    @Mock
    private ShoppingCartRepo shoppingCartRepo;

    @InjectMocks
    private ShoppingCartService shoppingCartService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetItems(){
        List<ShoppingCart> expectedItems = new ArrayList<>();
        when(shoppingCartRepo.findAll()).thenReturn(expectedItems);

        List<ShoppingCart> result = shoppingCartService.getItems();

        assertEquals(expectedItems, result);
    }

    @Test
    public void testSaveItem(){
        ShoppingCart item = new ShoppingCart();
        shoppingCartService.saveItem(item);

        verify(shoppingCartRepo, times(1)).save(item);
    }

    @Test
    public void testRemoveItem(){
        long itemId = 1L;
        shoppingCartService.removeItem(itemId);

        verify(shoppingCartRepo, times(1)).deleteById(itemId);
    }

    @Test
    public void testRemoveAllItems(){
        shoppingCartService.removeAllItems();

        verify(shoppingCartRepo, times(1)).deleteAll();
    }

    @Test
    public void testFindItemByName(){
        String itemName = "ItemName";
        ShoppingCart expectedItem = new ShoppingCart();
        when(shoppingCartRepo.findItemByName(itemName)).thenReturn(expectedItem);

        ShoppingCart result = shoppingCartService.findItemByName(itemName);

        assertEquals(expectedItem, result);
    }

    @Test
    public void testFindItemByName_ItemNotFound(){
        String itemName = "NonExistentItem";
        when(shoppingCartRepo.findItemByName(itemName)).thenReturn(null);

        ShoppingCart result = shoppingCartService.findItemByName(itemName);

        assertNull(result);
    }

    @Test
    public void testSaveItem_Failure(){
        ShoppingCart item = new ShoppingCart();
        when(shoppingCartRepo.save(item)).thenThrow(new RuntimeException("Failed to save"));

        // This test should ensure that exceptions are properly handled
        assertThrows(RuntimeException.class, () -> shoppingCartService.saveItem(item));
    }

    @Test
    public void testRemoveItem_ItemNotFound(){
        long itemId = 1L;
        doThrow(new EmptyResultDataAccessException(1)).when(shoppingCartRepo).deleteById(itemId);

        // This test should ensure that exceptions are properly handled
        assertThrows(EmptyResultDataAccessException.class, () -> shoppingCartService.removeItem(itemId));
    }
}



