package ie.atu.shopping_cart;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ShoppingCartApplicationTests.class)
class ShoppingCartApplicationTests {

	@Test
	void contextLoads() {
	}

}
