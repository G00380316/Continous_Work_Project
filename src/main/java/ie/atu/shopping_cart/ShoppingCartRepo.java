package ie.atu.shopping_cart;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ShoppingCartRepo extends JpaRepository<ShoppingCart, Long> {
    public ShoppingCart findItemByName(String name);
}
