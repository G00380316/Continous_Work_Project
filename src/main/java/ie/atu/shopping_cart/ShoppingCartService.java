package ie.atu.shopping_cart;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ShoppingCartService {

    private final ShoppingCartRepo shoppingCartRepo;

    public List<ShoppingCart> getItems() {
        return shoppingCartRepo.findAll();
    }

    public void saveItem(ShoppingCart item){
            shoppingCartRepo.save(item);
    }

    public void removeItem(long count){
        shoppingCartRepo.deleteById(count);
    }

    public  void removeAllItems(){
        shoppingCartRepo.deleteAll();
    }
    public ShoppingCart findItemByName(String name){
        return shoppingCartRepo.findItemByName(name);
    }
}
