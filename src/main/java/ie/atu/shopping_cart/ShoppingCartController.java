package ie.atu.shopping_cart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/shoppingcart")
public class ShoppingCartController {
    private final  ShoppingCartService cartService;

    @Autowired
    public ShoppingCartController(ShoppingCartService cartService){this.cartService = cartService;}

        @GetMapping
        public List<ShoppingCart> getItems(){
            return cartService.getItems();
        }

        @GetMapping("/remove/{id}")
        public void removeItem(@PathVariable("id") Long id){
            cartService.removeItem(id);
        }

        @GetMapping("/remove/clear")
        public void removeAllItems(){
        cartService.removeAllItems();
        }
        @PostMapping("")
        public void saveItem(@RequestBody ShoppingCart shoppingCart) {
            cartService.saveItem(shoppingCart);
        }

        @GetMapping("/name/{name}")
        public ShoppingCart findItemByName(@PathVariable("name") String name){
            return cartService.findItemByName(name);
        }
}
