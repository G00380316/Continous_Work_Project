package ie.atu.shopping_cart;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table
public class ShoppingCart {

    private String name;
    private double price;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    public ShoppingCart( String name, double price){
        setName(name);
        setPrice(price);
    }


    public void setName(String name){
        if(name.length() > 2){
            this.name = name;
        }
        else {
            throw new IllegalArgumentException("This is not a valid name. All Item contains names of 3 letters or above can be added");
        }
    }

    public void setPrice(double price){
        if(price > 0){
            this.price = price;
        }
        else {
            throw new IllegalArgumentException("This is not a valid price. All Item prices above 0 can be added ");
        }
    }
}


