# Continous_Work_Project
Research, design and build a high-quality CICD pipeline to generate a java micro services-based application(Shopping Cart).

[Useful SQl commands for PSQL Terminal(Written in order)]:

[(Runs Postgres Image through terminal in a Container that resides in a Network to allow for communication with other images/containers)]
docker run --name db -p 5432:5432 --network=db -v "%cd%:/var/lib/postgresql/data" -e  POSTGRES_PASSWORD=password -d postgres:alpine

[(Starts Postgres Image through terminal in a Container that resides in a Network to allow for communication with other images/containers)]
docker start db 

[(Starts Database and monitors database in Terminal)]
docker run -it --rm --network=db postgres:alpine psql -h db -U postgres

[(Creates Database)]
CREATE DATABASE cart;

[(list databases) in mode(postgres=#)]
\l

[(to connect to Database) in mode(postgres=#)]
\c atu

[Information on pushing code to Docker Repository]
1. If you want push code to Docker you must enter pom file and change the version like by +1 in the relevant decimal place
   like the comment says beside it 
2. If you want push manually type/paste this code ( mvn clean package dockerfile:push ) in the terminal and press enter 



